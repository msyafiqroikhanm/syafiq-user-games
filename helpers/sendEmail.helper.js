const nodemailer = require("nodemailer");
require("dotenv").config();

module.exports = async (from, to, subject, text, html) => {
  let transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: 2525,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASS,
    },
  });

  let info = await transporter.sendMail({
    from, // sender address
    to, // list of receivers
    subject, // Subject line
    text, // plain text body
    html,
  });

  return info;
};
