const swaggerAutogen = require("swagger-autogen")();

const doc = {
  info: {
    title: "User Game API",
    description:
      "This is an API for the user games service. The technology used in this API is expressJs and Postgresql database. Made to fulfill the challenge 05 binary academy. Developed by M. Syafiq Roikhan Maulana",
  },
  host: "localhost:3000",
  schemes: ["http"],
};

const outputFile = "./swagger-output.json";
const endpointsFiles = ["./routes/index.js", "./routes/user.routes.js"];

swaggerAutogen(outputFile, endpointsFiles, doc);
