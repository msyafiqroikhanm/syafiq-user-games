const jwt = require("jsonwebtoken");
const { user_game, user_game_biodata } = require("../models");
const bcrypt = require("bcryptjs");
require("dotenv").config();
const { OAuth2Client } = require("google-auth-library");
const axios = require("axios");
const {
  findUserAndBioBy,
  createUser,
  updateUser,
} = require("../services/userGame.services");
const {
  checkBioBy,
  findBioAndUserBy,
} = require("../services/userGameBiodata.services");
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);
const otpGenerator = require("otp-generator");
const sendEmail = require("../helpers/sendEmail.helper");
class AuthController {
  static async login(req, res, next) {
    // #swagger.tags = ['Authentication']
    // #swagger.summary = 'Access to API'
    /* #swagger.responses[200] = {
            description: 'OK',
            schema: {
                        "message": "Login Successfully",
                        "accessToken": "[your token]"
                    }
    } */
    /* #swagger.responses[401] = {
            description: 'Error: Unauthorized Request',
            schema: {
                        "message": "Invalid Username Or Password",
                    }
    } */
    /* #swagger.responses[500] = {
            description: 'Error: Not Found',
            schema: {
                        "message": "Internal Server Error",
                    }
    } */

    try {
      let token;
      if (req.body.username && req.body.passsword) {
        const user = await user_game.findOne({
          where: { username: req.body.username },
        });
        if (!user) {
          throw {
            status: 401,
            message: "Invalid Username Or Password",
          };
        }

        let passwordIsValid = bcrypt.compareSync(
          req.body.password,
          user.password
        );
        if (!passwordIsValid) {
          throw {
            status: 401,
            message: "Invalid Username Or Password",
          };
        }

        token = jwt.sign(
          {
            id: user.id,
            username: user.username,
          },
          process.env.JWT_PRIVATE_KEY,
          { expiresIn: 86400 }
        );
      } else {
        throw {
          status: 403,
          message: "Invalid username or password",
        };
      }

      res
        .status(200)
        .json({ message: "Login Successfully", accessToken: token });
    } catch (error) {
      next(error);
    }
  }

  static async loginWithGoogle(req, res, next) {
    try {
      let token, newUser;
      const google = await client.verifyIdToken({
        idToken: req.body.id_token,
        audience: process.env.GOOGLE_CLIENT_IDs,
      });
      const username = google.payload.given_name;

      const isExist = {
        username: await findUserAndBioBy({ username, auth: "google" }),
        email: await checkBioBy({ email: google.payload.email }),
      };

      if (isExist.email && isExist.username) {
        token = jwt.sign(
          {
            id: isExist.username.id,
            username: isExist.username.username,
          },
          process.env.JWT_PRIVATE_KEY
        );
        res
          .status(200)
          .json({ message: "Login Successfully", accessToken: token });
      } else {
        newUser = await createUser(
          {
            auth: "google",
            username: username,
          },
          {
            nickname: username,
            email: google.payload.email,
          }
        );
        token = jwt.sign(
          { id: newUser.id, username: newUser.username },
          process.env.JWT_PRIVATE_KEY
        );
        res
          .status(200)
          .json({ message: "Login Successfully", accessToken: token });

        await sendEmail(
          "noreply@game.com",
          google.payload.email,
          "Welcome to My Game",
          null,
          `<p>
            Welcome <b>${google.payload.name}</b> to My Games
          </p>`
        );
      }
    } catch (error) {
      next(error);
    }
  }

  static async loginWithFacebook(req, res, next) {
    try {
      let token, newUser;
      const url = `https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday%2Cpicture&access_token=${req.body.id_token}`;
      const facebook = await axios.get(url);

      const username = facebook.data.name.split(" ")[0];
      const isExist = {
        username: await findUserAndBioBy({ username, auth: "facebook" }),
        email: await checkBioBy({ email: facebook.data.email }),
      };

      if (isExist.email && isExist.username) {
        token = jwt.sign(
          {
            id: isExist.username.id,
            username: isExist.username.username,
          },
          process.env.JWT_PRIVATE_KEY
        );
        res
          .status(200)
          .json({ message: "Login Successfully", accessToken: token });
      } else {
        newUser = await createUser(
          {
            auth: "facebook",
            username: username,
          },
          {
            nickname: username,
            email: facebook.data.email,
          }
        );
        token = jwt.sign(
          { id: newUser.id, username: newUser.username },
          process.env.JWT_PRIVATE_KEY
        );
        res
          .status(200)
          .json({ message: "Login Successfully", accessToken: token });

        await sendEmail(
          "noreply@game.com",
          facebook.data.email,
          "Welcome to My Game",
          null,
          `<p>
              Welcome <b>${facebook.data.name}</b> to My Games
            </p>`
        );
      }
    } catch (error) {
      next(error);
    }
  }

  static async sendEmailForgotPassword(req, res, next) {
    try {
      const user = await findBioAndUserBy({ email: req.body.email });
      if (!user) {
        throw {
          status: 404,
          message: "Email not registered",
        };
      }

      const otp = otpGenerator.generate(6, {
        upperCaseAlphabets: false,
        specialChars: false,
      });
      const salt = bcrypt.genSaltSync(10);
      const otpHashed = bcrypt.hashSync(otp, salt);
      const expired = new Date(new Date().getTime() + 5 * 60000);

      await updateUser(user.User.id, {
        forgot_pass_token: otpHashed,
        forgot_pass_token_expiredAt: expired,
      });

      res.status(200).json({ message: "Please check your email" });

      await sendEmail(
        "noreply@game.com",
        user.email,
        "Forgot Password",
        null,
        `<p>Token: ${otp}</p <p>expired: ${expired}</p>`
      );
    } catch (error) {
      next(error);
    }
  }

  static async verifyForgotPassToken(req, res, next) {
    try {
      const user = await findBioAndUserBy({ email: req.body.email });
      if (!user) {
        throw {
          status: 404,
          message: "Email not registered",
        };
      }

      if (!user.User.forgot_pass_token) {
        throw {
          status: 401,
          message: "Invalid email or token",
        };
      }

      if (!bcrypt.compareSync(req.body.token, user.User.forgot_pass_token)) {
        throw {
          status: 401,
          message: "Invalid email or token",
        };
      }

      if (new Date() > user.User.forgot_pass_token_expiredAt) {
        throw {
          status: 401,
          message: "Your token is expired",
        };
      }

      res.status(200).json({ valid: true, message: "Your token is valid" });
    } catch (error) {
      next(error);
    }
  }

  static async changePassword(req, res, next) {
    try {
      const user = await findBioAndUserBy({ email: req.body.email });
      if (!user) {
        throw {
          status: 404,
          message: "Email not registered",
        };
      }

      if (!user.User.forgot_pass_token) {
        throw {
          status: 401,
          message: "Invalid email or token",
        };
      }

      if (!bcrypt.compareSync(req.body.token, user.User.forgot_pass_token)) {
        throw {
          status: 401,
          message: "Invalid email or token",
        };
      }

      if (new Date() > user.User.forgot_pass_token_expiredAt) {
        throw {
          status: 401,
          message: "Your token is expired",
        };
      }

      if (req.body.password !== req.body.password_confirm) {
        throw {
          status: 400,
          message: "Password confirmation doesnt match",
        };
      }

      const salt = bcrypt.genSaltSync(10);
      const hashedPassword = bcrypt.hashSync(req.body.password, salt);

      await updateUser(user.User.id, {
        passsword: hashedPassword,
        forgot_pass_token: null,
        forgot_pass_token_expiredAt: null,
      });

      res.status(200).json({ message: "Successfully update password" });
    } catch (error) {
      next(error);
    }
  }

  static async authorization(req, res, next) {
    /* #swagger.responses[401] = {
            description: 'Error: Unauthorized Request',
            schema: {
                        "message": "Unauthorized Request",
                    }
    } */
    /* #swagger.responses[401] = {
            description: 'Error: Unauthorized Request',
            schema: {
                        "message": "Unauthorized Request",
                    }
    } */

    try {
      if (!req.headers.authorization) {
        throw {
          status: 401,
          message: "Unauthorized Request",
        };
      }
      jwt.verify(
        req.headers.authorization,
        process.env.JWT_PRIVATE_KEY,
        function (err, decoded) {
          if (err) {
            throw {
              status: 401,
              message: "Unauthorized Request",
              err,
            };
          }
          req.user = decoded;
          next();
        }
      );
    } catch (error) {
      next(error);
    }
  }
}

module.exports = AuthController;
