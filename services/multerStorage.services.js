const multer = require("multer");
const fs = require("fs");
const path = "public/src";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }
    cb(null, path);
  },
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + "-" + file.originalname;
    cb(null, file.fieldname + "-" + uniqueSuffix);
  },
});

module.exports = storage;
