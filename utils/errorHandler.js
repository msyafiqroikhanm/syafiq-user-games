const multer = require("multer");

function errorHandler(err, req, res, next) {
  console.log(err);
  if (err.status) {
    res.status(err.status).json({ message: err.message });
  } else if (err instanceof multer.MulterError) {
    res.status(400).json({ message: err.message });
  } else {
    res.status(500).json({ message: "Internal Server Error" });
  }
}
module.exports = errorHandler;
