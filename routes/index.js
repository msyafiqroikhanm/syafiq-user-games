const router = require("express").Router();
const GameController = require("../controllers/GameController.js");
const AuthController = require("../controllers/AuthController.js");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../swagger-output.json");
const userRoutes = require("../routes/user.routes");
const { body, check, validationResult } = require("express-validator");
const multer = require("multer");
const storage = require("../services/multerStorage.services");
const upload = multer({
  storage,
  limits: { fileSize: 10000000 },
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "video/mp4" ||
      file.mimetype === "video/quicktime" ||
      file.mimetype === "	video/x-matroska"
    ) {
      cb(null, true);
    } else {
      cb(
        { status: 400, message: "file must be in .mp4,.mov, or .mkv format" },
        false
      );
    }
  },
});

router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
router.post("/login", AuthController.login);
router.post("/login-with-google", AuthController.loginWithGoogle);
router.post("/login-with-facebook", AuthController.loginWithFacebook);

router.post(
  "/register",

  upload.single("video_profile"),

  [
    body("username").notEmpty(),
    body("password").notEmpty(),
    body("nickname").notEmpty(),
    check("email", "Email Not Valid").isEmail(),
    check("mobile_no", "Mobile Number is Not valid").isMobilePhone("id-ID"),
  ],
  GameController.addUser
);

router.post(
  "/forgot-password",
  [body("email").notEmpty()],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: errors,
      };
    }
    next();
  },
  AuthController.sendEmailForgotPassword
);

router.post(
  "/verify-forgot-password",
  [body("email").notEmpty(), body("token").notEmpty()],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: errors,
      };
    }
    next();
  },
  AuthController.verifyForgotPassToken
);

router.post(
  "/change-password",
  [
    body("email").notEmpty(),
    body("token").notEmpty(),
    body("password").notEmpty(),
    body("password_confirm").notEmpty(),
  ],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: errors,
      };
    }
    next();
  },
  AuthController.changePassword
);

router.use(AuthController.authorization);

router.use("/users", userRoutes);

router.get("/histories", GameController.getHistories);

module.exports = router;
