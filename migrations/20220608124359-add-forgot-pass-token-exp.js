"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn("user_games", "forgot_pass_token", {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn(
      "user_games",
      "forgot_pass_token_expiredAt",
      {
        type: Sequelize.DATE,
      }
    );
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn("user_games", "forgot_pass_token", null);
    await queryInterface.removeColumn(
      "user_games",
      "forgot_pass_token_expiredAt",
      null
    );
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
};
