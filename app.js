const express = require("express");
const app = express();
const errorHandler = require("./utils/errorHandler");
const routes = require("./routes");
const morgan = require("morgan");
const path = require("path");

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(morgan("dev"));
app.use(express.static(path.join("public", "src")));

app.use(routes);

app.use(errorHandler);

module.exports = app;
